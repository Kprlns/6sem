from time import gmtime, strftime

# p = 10**4 + 7
p = 8893

a = 5236748245367423648747
b = 1276321389409853474565276387982097317

a %= p
b %= p
print('p = ' + str(p))
print('a = ' + str(a))
print('b = ' + str(b))


def belongs_to_curve(x, y):
    return (y ** 2) % p == ((x ** 3) % p + (a * x) % p + b) % p


def bypass():
    start = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    print(start)

    file = open("curve_p=" + str(p) + "_a=" + str(a) + "_b=" + str(b) +
                "_start=" + str(start).replace(" ", "_") + ".txt", "w")
    cnt = 0
    for i in range(-p + 1, p - 1):
        for j in range(-p + 1, p - 1):
            if belongs_to_curve(i, j):
                print(str(cnt) + ': ( ' + str(i) + ', ' + str(j) + ' )')
                file.write(str(cnt) + ': ( ' + str(i) + ', ' + str(j) + ' )\n')
                cnt += 1

    end = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    print('start: ' + start)
    print('end: ' + end)
    file.write('start: ' + start + '\n')
    file.write('end: ' + end + '\n')
    file.close()


bypass()
