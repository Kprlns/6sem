package main;

import java.math.BigInteger;
import java.util.Random;
import java.util.Vector;

public class Factorization {

    private BigInteger number;

    private final BigInteger two = new BigInteger("2");
    private final BigInteger one = new BigInteger("1");
    private final BigInteger zero = new BigInteger("0");



    public Factorization(String val) {
        number = new BigInteger(val);
    }

    public Factorization(String val1, String val2) {
        number = new BigInteger(val1);
        BigInteger num = new BigInteger(val2);

        number = num.multiply(number);
        System.out.println(number.toString() + "\n");
    }

    public Vector<BigInteger> pollardFactorization() {
        Vector<BigInteger> res = new Vector<>();

        res.add(findDevider());
        res.add(number.divide(res.get(0)));

        return res;
    }


    private BigInteger findDevider() {
        BigInteger
                x = bigIntRand(),
                y = new BigInteger("1"),
                i = new BigInteger("0"),
                stage = new BigInteger("2");

        BigInteger tmp = x.subtract(y);
        tmp = tmp.abs();
        tmp = tmp.gcd(number);
        while(tmp.equals(one)) {
            if(i.equals(stage)) {
                System.out.println(stage.toString());
                y = new BigInteger(x.toString());
                stage = stage.multiply(two);

                if(stage.compareTo(number.multiply(new BigInteger("16"))) >= 0) {
                    System.exit(5);
                }
            }

            x = x.multiply(x).mod(number);
            i = i.add(one);

            tmp = x.subtract(y);
            tmp = tmp.abs();
            tmp = tmp.gcd(number);
        }
        return tmp;
    }

    private BigInteger bigIntRand() {
        Random rand = new Random();
        BigInteger res = new BigInteger(number.bitLength() + 1,rand);

        BigInteger tmp = number.subtract(one);
        if(res.compareTo(tmp) >= 0) {
            res = res.mod(tmp);
        }
        if(res.compareTo(zero) <= 0) {
            res = new BigInteger("1");
        }

        return res;
    }
}












