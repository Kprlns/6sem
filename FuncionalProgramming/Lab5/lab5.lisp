(defgeneric sub2 (arg1 arg2)
  (:method ((n1 number) (n2 number))
  (- n1 n2)))


(defun make-term (&key order coeff)
  (list order coeff))

(defun order (term) (first term))   ; степень
(defun coeff (term) (second term))

(defclass polynom ()
 ((polunom-symbol :initarg :var1 :reader var1)
  ;; Разреженный список термов в порядке убывания степени
  (term-list :initarg :terms :reader terms)))
(defgeneric zerop1 (arg)
 (:method ((n number))   ; (= n 0)
  (zerop n)))

(defgeneric minusp1 (arg)
 (:method ((n number))   ; (< n 0)
  (minusp n)))

(defmethod print-object ((p polynom) stream)
  (format stream "[МЧ (~s) ~:{~:[~:[+~;-~]~d~[~2*~;~s~*~:;~s^~d~]~;~]~}]"
          (var1 p)
          (mapcar (lambda (term)
                    (list (zerop1 (coeff term))
                          (minusp1 (coeff term))
                          (if (minusp1 (coeff term))
                              (abs (coeff term))
                              (coeff term))
                          (order term)
                          (var1 p)
                          (order term)))
                  (terms p))))



(defmethod sub2 ((p1 polynom) (p2 polynom))
  (if (same-variable-p (var1 p1) (var1 p2))
      (make-instance 'polynom
                     :var1 (var1 p1)
                     :terms (sub-terms (terms p1)
                                       (terms p2)))
      (error "Многочлены от разных переменных: ~s и ~s"
             p1 p2)))



(defun same-variable-p (v1 v2)
  ;; Переменные v1 и v2 - совпадающие символы
  (and (symbolp v1) (symbolp v2) (eq v1 v2)))

(defun sub-terms (tl1 tl2)
  ;; Объединить списки термов tl1 и tl2,
  ;; упорядочивая по убыванию степеней
  (cond ((null tl1) tl2)
        ((null tl2) tl1)
        (t
         (let ((t1 (first tl1))
               (t2 (first tl2)))
           (cond ((> (order t1) (order t2))
                  (adjoin-term t1
                               (sub-terms (rest tl1) tl2)))
                 ((< (order t1) (order t2))
                  (adjoin-term (make-term :order (order t2)
                                          :coeff (- (coeff t2)));;t2
                               (sub-terms tl1 (rest tl2))))
                 (t
                  ;; степени совпадают - суммируем коэффициенты
                  (adjoin-term  
                   (make-term :coeff (sub2 (coeff t1) (coeff t2))
                              :order (order t1))
                   (sub-terms (rest tl1)
                              (rest tl2)))))))))


(defun adjoin-term (term term-list)
  ;; Добавить term к списку term-list
  (if (zerop1 (coeff term))   ; если коэффициент нулевой,
      term-list               ; то отбрасываем терм,
      (cons term term-list))) 



(make-instance 'polynom ; 5x^2 + 3.3x - 7
      :var1 'x
      :terms (list (make-term :order 2 :coeff 5)
                   (make-term :order 1 :coeff 3.3)
                   (make-term :order 0 :coeff -7)))

(make-instance 'polynom ; x^3 + 2x + 1
      :var1 'x
      :terms (list (make-term :order 3 :coeff 1)
                   (make-term :order 1 :coeff 2)
                   (make-term :order 0 :coeff 1)))
