(defun task(func list) (solve func list (list )))

(defun solve(func list res)
  (cond
    ((null list) res)
    ((null (member  (head func list) res))
    (solve func (rest list) (append res (list (head func list)))))
    (t (solve func (rest list) res))
    ))
(defun head(func list)  (funcall func (first list)))

(print (task (lambda (x) (- (* x x) 1)) (list 1 2 3 -1 -2 -3)))
(print (task #'abs (list 1 2 3 4 -1 -2 -3)))
(print (task #'abs (list 1 -1 -2 2 3 4 -1 -3)))
(print (task #'identity (list 20 20 30 30 40 40)))
(print (task #'identity (list )))


