(defun task( a b ) (cond ((or (= a 0) (= b 0)) 0)
                         ((< b 0)  (solve (- a) (- b) ))
                         (t (solve a b)))
       )

(defun solve(a b)
  (if
   (= b 1) a
   (if (= 0 (mod b 2))
                   (solve (* a 2) (/ b 2))
                   (+ (solve a (- b 1))  a) ))
  )
