(Defun Print-matrix (matrix &optional (chars 3) stream)
  (let ((*print-right-margin* (+ 6 (* (1+ chars)
                                      (array-dimension matrix 1)))))
    (pprint matrix stream)
    (values)))

(defun swap-lines (matrix j k)
  (format t "~%~%swapping lines: ~d ~d " j k)
  (loop for i upfrom 0 below (array-dimension matrix 1)
     do (psetf (aref matrix j i) (aref matrix k i)
                   (aref matrix k i) (aref matrix j i))))

(defun swap-columns (matrix j k)
  (format t "~%~%swapping columns: ~d ~d " j k)
  (loop for i upfrom 0 below (array-dimension matrix 0)
     do (psetf (aref matrix i j) (aref matrix i k)
               (aref matrix i k) (aref matrix i j))))

(defun copy-matrix (matrix)
  (let ((dimensions (array-dimensions matrix)))
    (adjust-array (make-array dimensions
                              :element-type (array-element-type matrix)
                              :displaced-to matrix)
                  dimensions)))


(defun find-min (matrix)
  (let ((min (aref matrix 0 0))
        (line 0)
        (column 0))
  (loop for i upfrom 0 below (array-dimension matrix 0)
     do (loop for j upfrom 0 below (array-dimension matrix 1)
             do (if (> min (aref matrix i j))
                    (progn (setq min (aref matrix i j))
                           (setq line i)
                           (setq column j)))))
  (values line column)))


(defun swap-min-to-bottom-left(matrix)
  (print-matrix matrix)
  (let  ((copy (copy-matrix matrix))
        (line 0)
        (column 0))
    (setf (values line column) (find-min matrix))
    (if (not (= line (- (array-dimension copy 0) 1)))
        (progn (swap-lines copy line (- (array-dimension copy 0) 1))
           (print-matrix copy)))
    (if (not (= column (- (array-dimension copy 1) 1)))
        (progn (swap-columns copy column (- (array-dimension copy 1) 1))
               (print-matrix copy)))
    (format t "~%~%source matrix:")
    (print-matrix matrix)
    copy)
  (format t "~%---------------~%"))

(swap-min-to-bottom-left #2A(
                             (2 -1)
                             (1 4)))

(swap-min-to-bottom-left #2A(
                             (2 1)
                             (-1 4)))

(swap-min-to-bottom-left #2A(
                             (1 2 0)
                             (2 0 1)
                             (0 2 1)))

(swap-min-to-bottom-left #2A((5 4 3)
                             (2 1 0)))

(swap-min-to-bottom-left #2A((5 -3 3)
                             (2 1 0)))
