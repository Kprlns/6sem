(defun whitespace-char-p (char)
  (member char '(#\Space #\Tab #\Newline)))

(defun word-list (string)
  ;; Разбить строки на слова, разделённые знаками whitespace
  ;; A la (split-seq-if #'whitespace-char-p string)
  (loop with len = (length string)
        for left = 0 then (1+ right)
        for right = (or (position-if #'whitespace-char-p string
                                     :start left)
                        len)
        unless (= right left)	; исключить пустые слова
          collect (subseq string left right)
     while (< right len)))

(defun text-to-list (text)
  (let ((words (list )))
    (dolist (sentence text)
      (setq words (append words (word-list sentence))))
    words))

(defun is-num (string)
  (let ((cnt 0))
    (loop for i upfrom 0 below (length string)
       do (if (and (> cnt -1) (digit-char-p (char string i)))
              (setq cnt (1+ cnt))
              (setq cnt -1)))
    (if (= cnt -1) NIL cnt)))

(defun max-digital-word-length (text)
  (let ((words (text-to-list text))
        (res-len 0)
        (res-word NIL)
        (current-len NIL))
    (dolist (current-word words)
      (progn
        (setq current-len (is-num current-word))
        (if (or (and current-len (>= current-len res-len)))
            (psetq
             res-len current-len
             res-word current-word))))
    (values res-len res-word)))


(max-digital-word-length '())

(max-digital-word-length '("Один 1 одиннадцать 11 пятнадцать 15"))

(max-digital-word-length '("Один 1 одиннадцать 11 пятнадцать 15" "abc 123 a12 1a3 12a"))

(max-digital-word-length '("Один 1 одиннадцать 11 пятнадцать 15" "abc 123 a12 1a3 12a" " 1234 4321 2314"))

