import pandas as pd
from sklearn.model_selection import train_test_split

from MyClassifier import MyClassifier


"""
Выбранный датасет- список ответов на вопросы по Python'у на StackOverflow
"""


bound = 10
n = 1000
dataset = pd.read_csv('pythonquestions/Answers.csv', encoding='utf-8', error_bad_lines=False, warn_bad_lines=False,
                      nrows=n)
# data = dataset.as_matrix(['Score', 'Body'])
score = dataset['Score']
body = dataset['Body']

x_train, x_test, y_train, y_test = train_test_split(body, score, test_size=0.2)

clf = MyClassifier(lower_bound=bound)

clf.fit(x_train, y_train, parsed=False)

y_pred = clf.predict(x_test, parsed=False)

cnt_correct = 0
cnt_error = 0

out = open('output.txt', 'w')
out.write('real  |  predicted\n')
for i in range(0, len(y_pred)):
    string = 'Bad'
    if y_test.iloc[i] >= bound:
        string = 'Good'
    if string == y_pred[i]:
        cnt_correct += 1
    else:
        cnt_error += 1
    out.write(str(y_test.iloc[i]) + ' ' + string + ' | ' + y_pred[i] + '\n')

sum = cnt_correct + cnt_error
out.write('\ncorrect: ' + str(cnt_correct) + ' ' + str(round(cnt_correct * 100. / sum, 2)) + '%\n')
out.write('error: ' + str(cnt_error) + ' ' + str(round(cnt_error * 100. / sum, 2)) + '%\n')
