import re
import copy
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model.logistic import LogisticRegression


class MyClassifier:
    __classifier = LogisticRegression()
    __lower_bound = 0
    __vectorizer = TfidfVectorizer()

    def __init__(self, lower_bound=20):
        self.__lower_bound = lower_bound

    @staticmethod
    def __parse_str(str):
        str = re.sub(r"(</.*>)", '', str)
        str = re.sub(r'(<.*>)', '', str)
        return re.sub(r'(\n+)', '\n', str)

    @staticmethod
    def __parse(arr):
        for i in range(0, len(arr)):
            arr.iloc[i] = MyClassifier.__parse_str(arr.iloc[i])
        return arr

    def __set_class(self, arr):
        for i in range(0, len(arr)):
            if arr.iloc[i] >= self.__lower_bound:
                arr.iloc[i] = 'Good'
            else:
                arr.iloc[i] = 'Bad'
        return arr

    def fit(self, data_x, data_y, parsed=True):
        data_x_copy = copy.copy(data_x)
        data_y_copy = copy.copy(data_y)
        y_train = self.__set_class(data_y_copy)
        if not parsed:
            data_x_copy = MyClassifier.__parse(data_x_copy)

        x_train = self.__vectorizer.fit_transform(data_x_copy)
        self.__classifier.fit(x_train, y_train)

    def predict(self, x, parsed=True):
        data_x = copy.copy(x)
        if not parsed:
            data_x = MyClassifier.__parse(data_x)

        test_x = self.__vectorizer.transform(data_x)
        return self.__classifier.predict(test_x)
