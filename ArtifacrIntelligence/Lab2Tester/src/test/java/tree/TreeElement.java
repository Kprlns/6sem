package tree;

public class TreeElement {
    public String text;
    public int parent;
    public int left;
    public int right;
    boolean leaf;
}
