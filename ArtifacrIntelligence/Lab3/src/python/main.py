import datetime
import re
import vk
from dateutil import parser, relativedelta
import cognitive_face as CF
import matplotlib.pyplot as plt
import numpy as np

COUNT = 10
ID = 24528807


def get_max_photo(user, n):
    res = 0
    arr = sorted(user['items'][n].keys())
    for i in range(len(arr)):
        m = re.search('\d+', arr[i])
        if m:
            if int(m.group(0)) > res:
                res = int(m.group(0))

    return user['items'][n]['photo_' + str(res)]


def count_user_friends(_api, user):
    return _api.friends.get(user_id=user['id'])['count']


session = vk.Session()
api = vk.API(session, v='5.35', lang='ru', timeout=100)

user = api.users.get(user_id=ID, fields='sex,bdate')
print(user)
all_friends = api.friends.get(user_id=ID, count=1000, fields='sex,bdate')
print(all_friends)

friends = []
cnt = 0
i = 0

while i < len(all_friends['items']) and cnt < COUNT:
    if 'bdate' in all_friends['items'][i]:
        if len(all_friends['items'][i]['bdate']) > 5 and not (all_friends['items'][i]['last_name'] == 'Winchester'):
            friends.append(all_friends['items'][i])
            cnt += 1
            i += 2  # 40
    i += 1

for i in range(len(friends)):
    age = parser.parse(friends[i]['bdate'])
    print(relativedelta.relativedelta(datetime.date.today(), age.date()).years)
    friends[i]['age'] = relativedelta.relativedelta(datetime.date.today(), age.date()).years

print(friends)
first = [0, 0]
num = [0, 0]
for i in range(len(friends)):
    first[friends[i]['sex'] - 1] += 1
    num[friends[i]['sex'] - 1] += count_user_friends(api, friends[i])

print(first)
print(num)

for i in range(len(num)):
    num[i] /= first[i]

x = np.arange(2)
plt.bar(x, num)
plt.title('Average number of friends depending on sex')
plt.xticks(x, ('Female', 'Male'))
plt.savefig('friends_by_sex.svg')

stat = {}
for i in range(len(friends)):
    tmp = friends[i]['age']
    if tmp in stat:
        stat[tmp][0] += count_user_friends(api, friends[i])
        stat[tmp][1] += 1
    else:
        stat[tmp] = [count_user_friends(api, friends[i]), 1]

print(stat)
x = []
num = []

for key, value in sorted(stat.items()):
    x.append(key)
    num.append(value[0] / value[1])

print(x)
print(num)
plt.clf()
plt.bar(x, num)
plt.title('Average number of friends depending on age')
plt.xticks(x, tuple(x))
plt.savefig('friends_by_age.svg')

face_api_key = "97489da06a2a443b9ce9bb8d0573b29d"
CF.Key.set(face_api_key)
face_api_url = "https://westeurope.api.cognitive.microsoft.com/face/v1.0"
CF.BaseUrl.set(face_api_url)

stat = {}
for i in range(len(friends)):
    tmp = friends[i]
    cnt = 0
    sum = 0
    photos = api.photos.get(owner_id=friends[i]['id'], album_id='profile', count=COUNT)
    for j in range(len(photos['items'])):
        print(get_max_photo(photos, j))
        reply = CF.face.detect(get_max_photo(photos, j))
        sum += len(reply)
        cnt += 1
    if cnt > 0:
        av = float(format(float(sum) / cnt, '.2f'))
        if av in stat:
            stat[av][0] += count_user_friends(api, friends[i])
            stat[av][1] += 1
        else:
            stat[av] = [count_user_friends(api, friends[i]), 1]

x = []
num = []

print(stat)

for key, value in sorted(stat.items()):
    x.append(key)
    num.append(value[0] / value[1])
print(x)
print(num)
plt.clf()
plt.bar(x, num, 0.05)
plt.title('Average number of friends depending on\n average number on friends on profile photo')
plt.savefig('friends_by_friends_on_photo.svg')
