import quandl as quandl
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model

dataset = quandl.get('Wiki/GOOGL')

tmpX = []
y = []
x = []

open = dataset[['Open']].as_matrix()
close = dataset[['Close']].as_matrix()

high = dataset[['High']].as_matrix()
low = dataset[['Low']].as_matrix()

for i in range(0, len(dataset)):
    tmpX.append(float(open[i]) - float(close[i]))
    y.append((float(high[i]) - float(low[i])))

plt.xlabel('Open - Close')
plt.ylabel('High - Close')
plt.plot(tmpX, y, 'ro')

for i in range(0, len(tmpX)):
    x.append([tmpX[i]])

regression = linear_model.LinearRegression()
regression.fit(x, y)


t = np.arange(-50, 61, 10)
plt.plot(t, regression.coef_[0] * t + regression.intercept_)
plt.savefig('shares_dataset.svg')
