import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn import linear_model
from math import isnan

pd.set_option('display.width', 100)

dataset = pd.read_csv('global_co2.csv')
columns = ['Year', 'Total', 'GasFuel', 'LiquidFuel', 'SolidFuel', 'Cement', 'GasFlaring', 'PerCapita']


def maxIndex(arr, length):
    pos = 0
    m = -1
    for j in range(0, length - 1):
        if m < arr[j]:
            m = arr[j]
            pos = j
    return pos


def regression(dataset, name):
    corrcoef = np.corrcoef(dataset.values.T)
    k = maxIndex(corrcoef[7], len(dataset.columns))

    y = dataset['PerCapita'].as_matrix()
    x = dataset[dataset.columns[k]].as_matrix()
    x = x.reshape(-1, 1)

    regr = linear_model.LinearRegression()
    regr.fit(x, y)

    plt.plot(x, y, 'ro')
    t = np.arange(dataset[dataset.columns[k]].min(), dataset[dataset.columns[k]].max(), 0.01)
    plt.plot(t, regr.coef_[0] * t + regr.intercept_)
    plt.title('r = {}, b = {}'.format(round(regr.coef_[0], 2), round(regr.intercept_, 2)))
    plt.ylabel('PerCapita')
    plt.xlabel(dataset.columns[k])

    plt.savefig('PerCapita_{}_{}.svg'.format(dataset.columns[k], name))
    plt.clf()


dataset_zero_nan = dataset.copy()
dataset_zero_nan['PerCapita'] = dataset_zero_nan['PerCapita'].replace(float('Nan'), 0)
regression(dataset_zero_nan, 'zero_nan')


cnt = 0
while isnan(dataset.iat[cnt, 7]):
    cnt += 1

dataset_without_nan = dataset.copy()
dataset_without_nan = dataset_without_nan.tail(len(dataset) - cnt)
regression(dataset_without_nan, 'without_nan')


dataset_average_nan = dataset.copy()
sum = 0
for i in range(cnt, len(dataset)):
    sum += dataset.iat[i, 7]

j = 0
average = sum / (len(dataset) - cnt)
dataset_average_nan['PerCapita'] = dataset_average_nan['PerCapita'].replace(float('Nan'), average)
regression(dataset_average_nan, 'average_nan')

dataset_min_nan = dataset.copy()
dataset_min_nan['PerCapita'] = dataset_min_nan['PerCapita'].replace(float('Nan'), dataset.iat[cnt, 7])
regression(dataset_min_nan, 'min_nan')