import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn import linear_model

dataset = pd.read_csv('challenge_dataset.txt')

x = dataset['first'].as_matrix()
y = dataset['second'].as_matrix()

plt.xlabel('first')
plt.ylabel('second')
plt.plot(x, y, 'ro')

regression = linear_model.LinearRegression()
regression.fit(x.reshape(-1, 1), y)

t = np.arange(3, 25, 1)
plt.plot(t, regression.coef_[0] * t + regression.intercept_)
plt.savefig('challenge_dataset.svg')

