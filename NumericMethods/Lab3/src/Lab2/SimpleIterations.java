package Lab2;

public class SimpleIterations {

    double eps;
    double left;
    double right;
    int count;
// [1, 2]
    SimpleIterations(double left, double right, double eps) {
        this.eps = eps;
        this.left = left;
        this.right = right;
        count = 0;
    }

    double f(double x) {
        return Math.sqrt((Math.pow(x, 3) - 10 * x + 15) / 2);
    }

    double df(double x) {
        double up = 3 * Math.pow(x, 2) - 10;
        double down = 2 * Math.sqrt(2 * (Math.pow(x, 3) - 10 * x + 15));
        return up / down;
    }

    double solve(double q) {
        double cur = left + (right - left) / 2;
        double prev;

        do {
            count++;
            prev = cur;
            cur = f(prev);
        } while ((Math.abs(cur - prev) * (q / (1 - q))) > eps);
        return cur;
    }
}
