package Lab2;

import Lab1.Pair;

public class Main {


    static final String NOI = "Number of iterations: ";
    public static void main(String[] args) {
        System.out.println("Newton method: ");
        NewtonMethod nm = new NewtonMethod(0.5, 0.01);
        System.out.println(nm.solve());
        System.out.println(NOI + nm.cnt);

        System.out.println("\nSimple iterations method: ");
        SimpleIterations si = new SimpleIterations(1, 2, 0.01);
        System.out.println(si.solve(0.6));
        System.out.println(NOI + si.count);

        System.out.println("\nNewton method for system: ");
        NewtonMethodSystem nms = new NewtonMethodSystem(1, 2, 1, 2, 0.01);
        Pair<Double> solution = nms.solve();

        System.out.println(solution.first());
        System.out.println(solution.second());
        System.out.println(NOI + nms.count);

        System.out.println("\nSimple iterations method for system: ");
        SimpleIterationSystem sis = new SimpleIterationSystem(1, 2, 1, 2, 0.01);
        solution = sis.solve();

        System.out.println(solution.first());
        System.out.println(solution.second());
        System.out.println(NOI + sis.count);
    }
}
