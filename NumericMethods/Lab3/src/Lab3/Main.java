package Lab3;

import Lab3.approximation.LessSquaresMethod;
import Lab3.derivation.Derivative;
import Lab3.integration.Integration;
import Lab3.interpolation.*;

import static Lab3.integration.Integration.rungeRomberg;
import static java.lang.Math.PI;

public class Main {

    public static void main(String[] args) {
        /*
        double[] first = new double[] {0, PI / 8, PI / 4, 3 * PI / 8};
        double[] second = new double[] {0, PI / 8, PI / 3, 3 * PI / 8};

        double point = 3 * PI / 16;

        LagrangeInterpolation li = new LagrangeInterpolation(first, 4);
        System.out.println(li.value(point));
        System.out.println(li.f(point));

        li.setPoints(second, 4);
        System.out.println("\n" + li.value(point));
        System.out.println(li.f(point) + "\n");

        NewtonIterpolation ni = new NewtonIterpolation(first, 4);
        System.out.println(ni.value(point));
        System.out.println(ni.f(point));


        ni.setPoints(second,4);
        System.out.println( "\n" + ni.value(point));
        System.out.println(ni.f(point) + "\n");

        double[] points = new double[] {0, 0.9, 1.8, 2.7, 3.6};
        double[] f = new double[] {0.0, 0.72235, 1.5609, 2.8459, 7.7275};
        CubicSpline cs = new CubicSpline(points, f, 5);

        System.out.println("------");
        System.out.println(cs.value(1.5));
        cs.print();


        //double[] x = new double[] {0, 1.7, 3.4, 5.1, 6.8, 8.5};
        //double[] y = new double[] {0.0, 1.3038, 1.8439, 2.2583, 2.6077, 2.9155};
        double[] x = new double[] {-0.9, 0, 0.9, 1.8, 2.7, 3.6};
        double[] y = new double[] {-1.2689, 0.0, 1.2689, 2.6541, 4.4856, 9.9138};

        LessSquaresMethod lsm = new LessSquaresMethod(x, y, 6, 1);
        System.out.println("Delta: " + lsm.delta());

        lsm = new LessSquaresMethod(x, y, 6, 2);
        System.out.println("Delta: " + lsm.delta());

        //x = new double[] {0, 0.1, 0.2, 0.3, 0.4};
        //y = new double[] {1.0, 1.1052, 1.2214, 1.3499, 1.4918};
        x = new double[] {1,2,3,4,5};
        y = new double[] {1, 2.6931, 4.0986, 5.3863, 6.6094};
        Derivative df = new Derivative(x, y, 5);
        System.out.println("\nFirst: " + df.firstDerivative(3));
        System.out.println("Second: " + df.secondDerivative(3));
        System.out.println("Left: " + df.leftDerivative(3));
        System.out.println("Right: " + df.rightDerivative(3));

        */
        Integration integr = new Integration(0, 2, 0.5);

        double ft = integr.trapezeMethod();
        double fs = integr.simpsonMethod();

        System.out.println("\nRectangle method: " + integr.rectangleMethod());
        System.out.println("Trapeze method: " + integr.trapezeMethod());
        System.out.println("Simpson method: " + integr.simpsonMethod());

        integr.setH(0.25);
        System.out.println("\nRectangle method: " + integr.rectangleMethod());
        System.out.println("Trapeze method: " + integr.trapezeMethod());
        System.out.println("Simpson method: " + integr.simpsonMethod());

        double st = integr.trapezeMethod();


        System.out.println("\nRunge-Romberg error: " + rungeRomberg(0.5,0.25, ft, st, 2));
        System.out.println("\nValue refined with Runge-Romgerg method: "
                + (rungeRomberg(0.5,0.25, ft, st, 2) + ft));
        System.out.println("                         Simpson method: " + fs);
    }
}

