package Lab3.integration;

import java.util.function.Function;

import static Lab3.integration.Function.f;

public class Integration {
    private double a;
    private double b;
    private double h;

    public Integration(double a, double b, double h) {
        this.a = a;
        this.b = b;
        this.h = h;
    }

    public double rectangleMethod() {
        double left = a;
        double right = a + h;

        double res = 0;
        while (right <= b) {
            res += f((left + right) / 2) * h;
            left = right;
            right += h;
        }
        return res;
    }

    public double trapezeMethod() {
        double left = a;
        double right = a + h;

        double res = 0;
        while (right <= b) {
            res += f(right) + f(left);
            left = right;
            right += h;
        }
        return res * h / 2;
    }

    public double simpsonMethod() {
        double res = 0;
        double left = a;
        double right = a + h;
        while (left < b) {
            res += (f(left) + 4 * f(left + h / 2) + f(left + h)) * h / 6;
            left += h;
            right += h;

            //res += f(right);
        }

        return res ;
    }


    public static double simpsonMethod(double a, double b, double h, Function<Double, Double> f) {
        double res = 0;
        double left = a;
        double right = a + h;
        while (left < b) {
            res += (f.apply(left) + 4 * f.apply(left + h / 2) + f.apply(left + h)) * h / 6;
            left += h;
            right += h;
            //res += f(right);
        }

        return res;

    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public void setH(double h) {
        this.h = h;
    }


    public static double rungeRomberg(double h1, double h2, double y1, double y2, double p) {
        double r = h2 / h1;

        return (y1 - y2) / (Math.pow(r, p) - 1.0);
    }
}
