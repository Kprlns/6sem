package Lab3.derivation;

public class Derivative {
    private double[] x;
    private double[] y;
    private int size;

    private double[][] coefficients;

    boolean counted;

    public Derivative(double[] x, double[] y, int size) {
        this.x = new double[size];
        this.y = new double[size];
        this.size = size;
        this.coefficients = new double[2][size - 1];

        counted = false;

        System.arraycopy(x, 0, this.x, 0, size);
        System.arraycopy(y, 0, this.y, 0, size);
    }


    private void count() {
        counted = true;
        for (int i = 0; i < size - 2; i++) {
            coefficients[0][i] = diff(i + 1);
            coefficients[1][i] = (diff(i + 2) - diff(i + 1)) / dx(i + 2, i);
        }
    }

    public double firstDerivative(double x) {
        if(!counted) {
            count();
        }
        int i = 0;
        while(this.x[i + 1] < x) {
            i++;
        }
        return coefficients[0][i] + coefficients[1][i] * (2*x - this.x[i] - this.x[i + 1]);
    }

    public double secondDerivative(double x) {
        if(!counted) {
            count();
        }
        int i = 0;
        while(this.x[i + 1] < x) {
            i++;
        }
        return 2 * coefficients[1][i];
    }

    public double leftDerivative(double x) {
        int i = 0;
        while(this.x[i] != x) {
            i++;
        }
        return dy(i) / dx(i);
    }

    public double rightDerivative(double x) {
        int i = 0;
        while(this.x[i] != x) {
            i++;
        }
        return dy(i + 1) / dx(i + 1);
    }


    private double diff(int i) {
        return dy(i) / dx(i);
    }

    private double dx(int i) {
        return x[i] - x[i - 1];
    }

    private double dy(int i) {
        return y[i] - y[i - 1];
    }

    private double dx(int first, int second) {
        return x[first] - x[second];
    }

}
