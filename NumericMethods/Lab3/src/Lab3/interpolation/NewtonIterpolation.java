package Lab3.interpolation;

public class NewtonIterpolation {
    double[][] k;
    double[] points;
    int size;
    boolean counted;

    public NewtonIterpolation(double[] points, int size) {
        //this.points = points;
        this.size = size;
        this.points = new double[size];
        System.arraycopy(points, 0, this.points, 0, size);

        k = new double[size][];
        for (int i = 0; i < size; i++) {
            k[i] = new double[size - i];
        }
        counted = false;
    }

    void count() {
        counted = true;
        for (int i = 0; i < size; i++) {
            k[0][i] = f(points[i]);
        }

        for (int i = 1; i < size; i++) {
            for (int j = 0; j < size - i; j++) {
                k[i][j] = (k[i - 1][j] - k[i - 1][j + 1]) /
                        (points[j] - points[j + i]);
            }
        }
    }

    public double value(double x) {
        if(!counted) {
            count();
        }
        double res = k[0][0];
        double tmp = 1;
        for (int i = 1; i < size; i++) {
            tmp *= (x - points[i - 1]);
            res += k[i][0] * tmp;
        }
        return res;
    }


    public double f(double x) {
        //return Math.tan(x) + x;
        //return Math.log(x);
        return Math.sin(Math.PI * x / 6);
    }

    public void setPoints(double[] points, int size) {
        this.points = points;
        this.size = size;
        for (int i = 0; i < size; i++) {
            k[i] = new double[size - i];
        }
        counted = false;
    }
}
