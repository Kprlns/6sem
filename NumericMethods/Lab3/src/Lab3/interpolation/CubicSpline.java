package Lab3.interpolation;

import Lab1.Matrix;
import Lab1.TridiagonalSolver;

import java.util.ArrayList;

public class CubicSpline {
    double[] points;
    double[] f;
    double[][] table;
    int size;
    boolean counted;

    final static int A = 0;
    final static int B = 1;
    final static int C = 2;
    final static int D = 3;

    public CubicSpline(double[] points, double[] f, int size) {
        //this.points = points;
        this.points = new double[size];
        System.arraycopy(points, 0, this.points, 0, size);

        //this.f = f;
        this.f = new double[size];
        System.arraycopy(f, 0, this.f, 0, size);
        this.size = size;
        table = new double[size - 1][4];
        counted = false;
        count();
    }

    public void print() {
        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    public double value(double x) {
        if(!counted) {
            count();
        }
        int i = 0;
        while(x >= points[i]) {
            i++;
        }
        i--;


        double k = 1;
        double res = 0;
        for (int j = 0; j < 4; j++) {
            res += table[i][j] * k;
            k *= (x - points[i]);
        }

        return res;
    }

    void count() {
        counted = true;
        countC();
        countABD();
/*
        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
        */
    }

    private void countABD() {
        for (int i = 0; i < size - 2; i++) {
            table[i][A] = f[i];
            table[i][B] = (f[i + 1] - f[i]) / h(i + 1) - h(i + 1) * (C(i + 1) + 2 * C(i)) / 3;
            table[i][D] = (C(i + 1) - C(i)) / (3 * h(i + 1));
        }

        table[size - 2][A] = f[size - 2];
        table[size - 2][B] = (f[size - 1] - f[size - 2]) / h(size - 1) - 2 * h(size - 1) * C(size - 2) / 3;
        table[size - 2][D] = -C(size - 2) / (3 * h(size - 1));
    }

    private void countC() {
        double[][] m = new double[size - 2][size - 2];

        int size = this.size - 2;
        m[0][0] = 2 * (h(1) + h(2));
        m[0][1] = h(2);
        for (int i = 3; i < this.size - 1; i++) {
            m[i - 2][i - 3] = h(i - 1);
            m[i - 2][i - 2] = 2 * (h(i - 1) + h(i));
            m[i - 2][i - 1] = h(i);
        }
        m[size - 1][size - 2] = h(this.size - 1);
        m[size - 1][size - 1] = 2 * (h(this.size - 1) + h(this.size - 2));



        ArrayList<Double> b = new ArrayList<>();
        for (int i = 2; i < this.size; i++) {
            b.add(3 * ((f[i] - f[i - 1]) / h(i) - (f[i - 1] - f[i - 2]) / h(1)) );
        }
        /*
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(m[i][j] + " ");
            }
            System.out.println(b.get(i));
        }
*/
        TridiagonalSolver ts = new TridiagonalSolver(new Matrix(m, size), b);
        ArrayList<Double> solution = ts.solve();
        //for (int i = 0; i < size; i++) {
        //    System.out.println(solution.get(i));
        //}

        table[0][C] = 0;
        for (int i = 0; i < solution.size(); i++) {
            table[i + 1][C] = solution.get(i);
        }
    }

    double h(int i) {
        return points[i] - points[i - 1];
    }

    double A(int i) {
        return table[i][A];
    }

    double B(int i) {
        return table[i][B];
    }

    double C(int i) {
        return table[i][C];
    }

    double D(int i) {
        return table[i][D];
    }
}
