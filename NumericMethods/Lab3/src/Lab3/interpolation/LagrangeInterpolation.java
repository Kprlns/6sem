package Lab3.interpolation;

public class LagrangeInterpolation {
    private double[] k;
    private double[] points;
    private int size;
    private boolean counted;


    public LagrangeInterpolation(double[] points, int size) {
        this.points = new double[size];//points;
        System.arraycopy(points, 0, this.points, 0, size);
        this.size = size;
        k = new double[size];
        counted = false;
    }

    private void count() {
        for (int i = 0; i < size; i++) {
            double w = 1;
            for (int j = 0; j < size; j++) {
                if(i != j) {
                    w *= (points[i] - points[j]);
                }
            }
            k[i] = f(points[i]) / w;
        }
    }

    public double value(double x) {
        if(!counted) {
            counted = true;
            count();
        }
        double res = 0;
        for (int i = 0; i < size; i++) {
            double tmp = k[i];
            for (int j = 0; j < size; j++) {
                if(i != j) {
                    tmp *= (x - points[j]);
                }
            }
            res += tmp;
        }
        return res;
    }

    public double f(double x) {
        return Math.tan(x) + x;
        //return Math.log(x);
    }

    public void setPoints(double[] points, int size) {
        this.points = points;
        this.size = size;
        k = new double[size];
        counted = false;
    }
}
