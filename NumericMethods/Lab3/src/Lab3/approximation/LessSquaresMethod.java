package Lab3.approximation;

import Lab1.Matrix;

import java.util.ArrayList;

public class LessSquaresMethod {
    private double[] x;
    private double[] y;
    private int size;
    private int power;

    private double[][] pows;
    private boolean[][] counted;
    boolean found;
    ArrayList<Double> polynom;
    Matrix matrix;
    ArrayList<Double> b;

    public LessSquaresMethod(double[] x, double[] y, int size, int power) {
        //this.x = x;
        this.x = new double[size];
        System.arraycopy(x, 0, this.x, 0, size);
        //this.y = y;
        this.y = new double[size];
        System.arraycopy(y, 0, this.y, 0, size);
        this.size = size;
        this.power = power;
        found = false;
        pows = new double[power * 2 + 1][size];
        counted = new boolean[power * 2 + 1][size];
        pows[0] = x;
        for (int i = 0; i < size; i++) {
            counted[0][i] = true;
        }
        for (int i = 1; i < power + 2; ++i) {
            for (int j = 0; j < size; ++j) {
                counted[i][j] = false;
            }
        }
    }

    private double sumX(int pow) {
        double res = 0;
        for (int i = 0; i < size; i++) {
            if(!counted[pow][i]) {
                pows[pow][i] = pows[pow - 1][i] * x[i];
                counted[pow][i] = true;
            }
            res += pows[pow][i];
        }
        return res;
    }

    private double sumXY(int pow) {
        double res = 0;
        for (int i = 0; i < size; i++) {
            res += pows[pow][i] * y[i];
        }
        return res;
    }

    public void genPolynom() {
        found = true;
        double[][] arr = new double[power + 1][power + 1];
        ArrayList<Double> b = new ArrayList<>();

        double sum = 0;
        for (int i = 0; i < size; i++) {
            sum += y[i];
        }

        b.add(sum);
        arr[0][0] = size;
        for (int i = 1; i < power + 1; ++i) {
            arr[0][i] = sumX(i - 1);
            for (int j = 0; j < power + 1; ++j) {
                arr[i][j] = sumX(i + j - 1);
            }
            b.add(sumXY(i - 1));
        }
        this.b = b;
        matrix = new Matrix(arr, power + 1);
        /*
        matrix.print();
        for (int i = 0; i < power + 1; i++) {
            System.out.println(b.get(i));
        }
        */
        polynom = matrix.solveSystem(b);
        System.out.print("\nPolynom сoefficients: ");
        for (int i = 0; i < polynom.size(); i++) {
            System.out.print(polynom.get(i) + " ");
        }
        System.out.println();
    }

    private double value(double x) {
        if(!found) {
            genPolynom();
        }
        double res = polynom.get(0);
        double curX = x;
        for (int i = 1; i < polynom.size(); i++) {
            res += curX * polynom.get(i);
            curX *= x;
        }

        return res;
    }

    public double delta() {
        double delta = 0;
        for (int i = 0; i < size; i++) {
            delta += Math.pow(value(x[i]) - y[i], 2);
        }
        return delta;
    }
}
