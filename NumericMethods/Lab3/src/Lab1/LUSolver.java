package Lab1;

import java.util.ArrayList;

public class LUSolver {
    private Matrix lower;
    private Matrix upper;
    private Matrix system;
    int size;

    public LUSolver(Pair<Matrix> pair, Matrix s) {

        lower = pair.first();
        upper = pair.second();
        system = s;
        size = system.size;
    }


    ArrayList<Double> solve(ArrayList<Double> b) {
        b = system.vectorSwap(b);

        ArrayList<Double> res = lowerSolver(b);
        res = upperSolver(res);
        //res = system.vectorReverseSwap(res);
        return res;
    }

    private ArrayList<Double> lowerSolver(ArrayList<Double> b) {
        ArrayList<Double> y = new ArrayList<>();
        double[][] lower = this.lower.matrix;

        if(lower[0][0] != 0) {
            y.add(b.get(0) / lower[0][0]);
        }

        for (int i = 1; i < system.size; i++) {

            double sum = 0;

            for (int j = 0; j < i; j++) {
                if(lower[i][j] != 0 && y.get(j) != 0) {
                    sum += lower[i][j] * y.get(j);
                }
            }

            if(lower[i][i] == 0) {
                if( (b.get(i) - sum) == 0 ) {
                    y.add(0.0);
                    continue;
                } else  {
                    throw new RuntimeException("No solutions");
                }
            }
            y.add( (b.get(i) - sum) / lower[i][i]);
        }
        return y;
    }

    private ArrayList<Double> upperSolver(ArrayList<Double> y) {
        ArrayList<Double> x = new ArrayList<>();
        for (int i = 0; i < y.size();i++) {
            x.add(0.0);
        }

        double[][] upper = this.upper.matrix;

        int size = system.size;
        if(upper[size - 1][size - 1] != 0) {
            x.set(y.size() - 1, y.get(y.size() - 1) / upper[size - 1][size - 1]);
        }
        for (int i = system.size - 1; i >= 0; i--) {
            double sum = 0;
            for (int j = i + 1; j < size; j++) {
                if(upper[i][j] != 0 && x.get(j) != 0) {
                    sum += upper[i][j] * x.get(j);
                }
            }

            if(upper[i][i] == 0) {
                if(y.get(i) == 0) {
                    y.add(0.0);
                    continue;
                } else {
                    //this.upper.print();
                    throw new RuntimeException("No solutions");
                }
            }
            x.set(i, (y.get(i) - sum) / upper[i][i]);
        }
        return x;
    }

    public double determinant() {
        double res = 1;
        double[][] upper = this.upper.matrix;
        for (int i = 0; i < size; i++) {
            res *= upper[i][i];
        }
        return res;
    }


}
