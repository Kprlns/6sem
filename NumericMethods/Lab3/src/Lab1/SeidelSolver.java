package Lab1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class SeidelSolver {

    private Matrix matrix;
    private double precision;
    private ArrayList<Double> b;
    int count;

    SeidelSolver(Matrix m, ArrayList<Double> b, double prec) {
        matrix = m;
        this.b = b;
        precision = prec;
        count = 0;
    }

    boolean precision(double[] x, double[] y) {
        double sum = 0;
        for (int i = 0; i < matrix.size; i++) {
            sum += (y[i] - x[i]) * (y[i] - x[i]);
        }
        return Math.sqrt(sum) >= precision;
    }

    ArrayList<Double> solve() {
        int n = matrix.size;
        double[][] matrix = this.matrix.matrix;
        double[] x = new double[n],
                 y = new double[n];
        do {

            for (int i = 0; i < n; i++) {
                x[i] = y[i];
            }

            for (int i = 0; i < n; i++) {
                double res = 0;

                for (int j = 0; j < i; j++) {
                    res += matrix[i][j] * y[j];
                }
                for (int j = i + 1; j < n; j++) {
                    res += matrix[i][j] * x[j];
                }

                y[i] = (b.get(i) - res) / matrix[i][i];
            }
            count++;
        } while (precision(x,y));
        ArrayList<Double> ret = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            ret.add(y[i]);
        }

        return ret;
    }

    ArrayList<Double> notSeidel() {
        int n = matrix.size;
        count = 0;
        //double[][] matrix = this.matrix.matrix;
        double[] x = new double[n],
                y = new double[n];

        double[][] matrix = new double[n][n];
        for (int i = 0; i < n; i++) {
            System.arraycopy(this.matrix.matrix[i], 0, matrix[i], 0, n);
        }

        do {

            System.arraycopy(y, 0, x, 0, n);

            for (int i = 0; i < n; i++) {
                double res = 0;

                for (int j = 0; j < n; j++) {
                    if(j != i) {
                        res += matrix[i][j] * x[j];
                    }
                }
                y[i] = (b.get(i) - res) / matrix[i][i];
            }
            count++;
        } while (precision(x,y));
        ArrayList<Double> ret = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            ret.add(y[i]);
        }
        return ret;
    }

}
