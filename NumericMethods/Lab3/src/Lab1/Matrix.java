package Lab1;

import java.util.ArrayList;

public class Matrix {
    double[][] matrix;
    int size;
    private ArrayList<Pair<Integer>> swaps;

    public Matrix(double[][] m, int size) {
        matrix = new double[size][size];
        this.size = size;
        for (int i = 0; i < size; i++) {
            System.arraycopy(m[i], 0, matrix[i], 0, size);
        }
        swaps = new ArrayList<Pair<Integer>>();

    }

    public Matrix() {
        swaps = new ArrayList<>();
    }

    public Matrix(int size) {
        this.size = size;
        matrix = new double[size][size];
        swaps = new ArrayList<>();
    }

    public double get(int i, int j) {
        return matrix[i][j];
    }

    public void set(int i, int j, double k) {
        matrix[i][j] = k;
    }

    Matrix(double[][] m, int size, ArrayList<Pair<Integer>> swps) {
        matrix = new double[size][size];
        this.size = size;
        for (int i = 0; i < size; i++) {
            System.arraycopy(m[i], 0, matrix[i], 0, size);
        }
        swaps = swps;

    }

    Matrix multiply(Matrix matrix) {

        double[][] res = new double[size][size];

        double[][] a = this.matrix;
        double[][] b = matrix.matrix;

        if (size != matrix.size) throw new RuntimeException("Illegal matrix dimensions.");

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                for (int k = 0; k < size; k++) {
                    res[i][j] += this.get(i, k) * matrix.get(i, k);//a[i][k] * b[k][j];
                }
            }
        }
        return new Matrix(res, size);
    }

    Pair<Matrix> QRdecompostion() {
        Matrix h = Matrix.E(size);
        Matrix m = this;


        for (int i = 0; i < size - 1; i++) {
            double[] vector = new double[size];

            double sum = 0;
            for (int j = i; j < size; j++) {
                sum += m.get(j, i) * m.get(j, i);//m.matrix[j][i] * m.matrix[j][i];
            }

            vector[i] = m.get(i, i) + Math.signum(m.get(i, i)) * Math.sqrt(sum);//m.matrix[i][i] + Math.signum(m.matrix[i][i]) * Math.sqrt(sum);

            for (int j = i + 1; j < size; j++) {
                vector[j] = m.get(i, j);//m.matrix[j][i];
            }
            for (int j = 0; j < size; j++) {
                //System.out.print(vector[j] + " ");
            }
            //System.out.println("\n------------------------");

            double n = 0;
            for (int j = 0; j < size; j++) {
                n += vector[j] * vector[j];
            }
            n = 2 / n;

            double[][] matr = new double[size][size];
            for (int k = 0; k < size; k++) {
                for (int l = 0; l < size; l++) {
                    matr[k][l] = n * vector[k] * vector[l];
                }
            }
            Matrix tmpH = Matrix.E(size).sub(new Matrix(matr,size));
            //tmpH.print();
            //System.out.println("________");
            m = tmpH.multiply(m);
            h = h.multiply(tmpH);
        }
        //h.print();
        //System.out.println("________");
        //m.print();
        //System.out.println("________");
        return new Pair<>(h, m);
    }

    Matrix sub(Matrix m) {
        double[][] res = new double[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                res[i][j] = this.get(i, j) - m.get(i, j);//matrix[i][j] - m.matrix[i][j];
            }
        }
        return new Matrix(res,size);
    }


    final Pair<Matrix> LUdecompostion() {
        double[][] matrix = new double[size][size];
        for (int i = 0; i < size; i++) {
            System.arraycopy(this.matrix[i], 0, matrix[i], 0, size);
        }
        double[][] l = new double[size][size],
                u = new double[size][size];

        for (int i = 0; i < size; i++) {

            int c = columnMax(this.matrix, i);
            if (i != c) {
                swaps.add(new Pair<>(i, c));
                swapLines(matrix, i, c);
                swapLines(u, i, c);
                swapLines(l, i, c);
            }
            for (int j = 0; j < size; j++) {
                u[0][i] = matrix[0][i];
                l[i][0] = matrix[i][0] / u[0][0];

                double sum = 0;
                for (int k = 0; k < i; k++) {
                    sum += l[i][k] * u[k][j];
                }
                u[i][j] = matrix[i][j] - sum;

                if (i > j) {
                    l[j][i] = 0;
                } else {
                    sum = 0;
                    for (int k = 0; k < i; k++) {
                        sum += l[j][k] * u[k][i];
                    }
                    l[j][i] = (matrix[j][i] - sum) / u[i][i];

                }
            }
        }
        return new Pair<>(new Matrix(l, size, this.swaps), new Matrix(u, size, this.swaps));
    }

    final private int columnMax(double[][] matrix, int j) {
        double max = Integer.MIN_VALUE;
        int max_i = j;
        for (int i = j; i < size; i++) {
            if (max < Math.abs(get(i, j))) {
                max = Math.max(max, Math.abs(get(i, j)));
                max_i = i;
            }
        }
        return max_i;
    }

    private void swapLines(double[][] matrix, int i, int j) {
        //for (int k = 0; k < size; k++) {
            //double tmp = get(i, k);//matrix[i][k];
            //set(i, k, get(j, k));//matrix[i][k] = matrix[j][k];
            //matrix[j][k] = tmp;
        //}
        double[] tmp = matrix[i];
        matrix[i] = matrix[j];
        matrix[j] = tmp;
    }

    void swapColumns(double[][] matrix, int i, int j) {
        for (int k = 0; k < size; k++) {
            double tmp = get(k, i);//matrix[k][i];
            set(k, i, get(k, j));//matrix[k][i] = matrix[k][j];
            set(k, j, tmp);//matrix[k][j] = tmp;
        }
    }

    /*
        public Vector<Double> solveLower(Vector<Double> b) {

        }
    */
    final ArrayList<Double> vectorSwap(ArrayList<Double> vector) {
        for (Pair<Integer> swap : swaps) {
            double tmp = vector.get(swap.first());
            vector.set(swap.first(), vector.get(swap.second()));
            vector.set(swap.second(), tmp);
        }
        return vector;
    }

    final public ArrayList<Double> vectorReverseSwap(ArrayList<Double> vector) {
        for (int i = swaps.size() - 1; i >= 0; i--) {
            double tmp = vector.get(swaps.get(i).first());
            vector.set(swaps.get(i).first(), vector.get(swaps.get(i).second()));
            vector.set(swaps.get(i).second(), tmp);
        }
        return vector;
    }


    public void print() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    final void doSwaps() {
        for (Pair<Integer> swap : swaps) {
            swapLines(this.matrix, swap.first(), swap.second());
            //swapColumns(this.matrix, swaps.get(i).first(), swaps.get(i).second());
        }
    }

    final void doRevarseSwap() {
        for (int i = swaps.size() - 1; i >= 0; i--) {
            swapLines(this.matrix, swaps.get(i).first(), swaps.get(i).second());
        }
    }

    public Matrix inversion() {

        this.print();
        double[][] res = new double[size][size];
        double[][] copy = new double[size][size];

        ArrayList<Pair<Integer>> swaps = new ArrayList<Pair<Integer>>();

        for (int i = 0; i < size; i++) {
            System.arraycopy(matrix[i],0, copy[i], 0, size);
            res[i][i] = 1;
        }
        new Matrix(copy,size).print();

        for (int i = 0; i < size - 1; i++) {
            int m = columnMax(copy, i);
            swapLines(copy, i, m);
            swapLines(res, i, m);
            swaps.add(new Pair<Integer>(i,m));
            //System.out.println(i + " " + m);

            for (int j = i + 1; j < size; j++) {
                double k = copy[j][i] / copy[i][i];
                subLine(copy[i],copy[j], k);
                subLine(res[i], res[j], k);

            }

        }
        //System.out.println("");
        //new Matrix(copy,size).print();

        for (int i = size - 1; i > 0; i--) {
            for (int j = i - 1; j >= 0; j--) {
                double k = copy[j][i] / copy[i][i];
                subLine(res[i], res[j], k);
            }
        }

        for (int i = 0; i < size; i++) {
            for(int j = 0; j < size; j++) {
                res[i][j] /= copy[i][i];
            }
        }

        Matrix ret = new Matrix(res,size);
        ret.swaps = swaps;
        //ret.doRevarseSwap();
        return ret;
    }

    final private void subLine(double[] src, double[] dest, double k) {
        for (int i = 0; i < size; i++) {
            if(src[i] != 0) {
                dest[i] -= src[i] * k;
            }
        }
    }

    public Matrix transpose() {
        double[][] matrix = new double[size][size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j <= i; j++) {
                matrix[i][j] = get(j, i);//this.matrix[j][i];
                matrix[j][i] = get(i, j);//this.matrix[i][j];
            }
        }

        return new Matrix(matrix,size);
    }

    static Matrix E(int size) {
        //double[][] ret =  new double[size][size];
        Matrix ret = new Matrix(size);
        for (int i = 0; i < size; i++) {
            //ret[i][i] = 1;
            ret.set(i, i, 1);
        }
        //return new Matrix(ret,size);
        return ret;
    }

    final public double determinant() {
        double res = 1;

        Matrix upper = this.LUdecompostion().second();
        double[][] matrix = upper.matrix;
        for (int i = 0; i < size; i++) {
            res *= matrix[i][i];
        }

        return res;
    }

    final public ArrayList<Double> solveSystem(ArrayList<Double> b) {
        return new LUSolver(this.LUdecompostion(), this).solve(b);
    }
}
