package Lab1;

import java.util.ArrayList;

public class EigenvalueFinder {
    private double precision;
    private Matrix m;
    private int size;
    private ArrayList<Matrix> u;
    private ArrayList<Matrix> uT;
    private int counter;

    EigenvalueFinder(Matrix m, double precision) {
        this.m = new Matrix(m.matrix,m.size);
        this.precision = precision;
        size = m.size;
        counter = 0;

        u = new ArrayList<>();
        uT = new ArrayList<>();
        System.out.println(precision);
    }

    void solve() {
        findU();

        Matrix tmp = u.get(0);

        for (int i = 1; i < u.size(); i++) {
            tmp = tmp.multiply(u.get(i));
        }

    }

     void findU() {
        Matrix kek = Matrix.E(size);

        while (precision()){
            Pair<Integer> pair = findMax();
            int i = pair.first();
            int j = pair.second();

            Matrix rotation = Matrix.E(size);

            double phi = Math.atan(2 * m.matrix[i][j] / (m.matrix[i][i] - m.matrix[j][j])) / 2;

            rotation.matrix[i][i] = Math.cos(phi);
            rotation.matrix[i][j] = -1 * Math.sin(phi);
            rotation.matrix[j][i] = Math.sin(phi);
            rotation.matrix[j][j] = Math.cos(phi);

            kek = kek.multiply(rotation);

            System.out.println("_______________");
            Matrix tmp = rotation.transpose().multiply(m);
            m = tmp.multiply(rotation);
            counter++;
            //m.print();
            System.out.println("_______________ " + counter);


        }
        kek.print();
        double[][] matr = kek.matrix;
        for (int i = 0; i < size - 1; i++) {

            for (int j = i + 1; j < size; j++) {
                double k = 0;

                for(int l = 0; l < size; l++) {
                    k += matr[i][l] * matr[j][l];
                }
                System.out.println(k);
            }
        }
        m.print();
    }

    boolean precision() {
        double sum = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < i; j++) {
                //System.out.print(m.matrix[i][j] + " ");
                sum += m.matrix[i][j] * m.matrix[i][j];
            }
            //System.out.println("");
        }
        System.out.println("prec * " + Math.sqrt(sum) + " *");
        return Math.sqrt(sum) > precision;
    }

    private Pair<Integer> findMax() {
        Pair<Integer> pair = new Pair<>(1, 0);
        double[][] matrix = m.matrix;
        double max = Math.abs(matrix[1][0]);
        for (int i = 0; i < size; i++) {
            for(int j = i + 1; j < size; j++) {
                double cur = Math.abs(matrix[i][j]);
                if(cur > max) {
                    max = cur;
                    pair.set(i,j);
                }
            }
        }
        System.out.println("max - " + max + " -" );
        return pair;
    }
}
