package Lab4.BoundaryTask;

import Lab1.Matrix;
import Lab4.Functions.MyFunction;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.function.Function;
import Lab1.TridiagonalSolver;

import static Lab4.CauchyTask.Methods.rungeKuttaMethod;

public class Methods {
    public static double[] finiteDifferenceMethod(double left, double right, double h,
                                                  double yLeft, double yRight,
                                                  double alpha, double beta,
                                                  Function<Double, Double> p, Function<Double, Double> q, Function<Double, Double> f)
    {
        LinkedList<Double> x = new LinkedList<>();
        for (double cur = left; cur <= right + h/2; cur += h) {
            x.add(cur);
        }
        int n = (int)((right - left) / h) + 1;
        double[][] matrix = new double[n][n];
        ArrayList<Double> v = new ArrayList<>();

        double x0 = x.get(0);
        matrix[0][0] = -1 / h;
        matrix[0][1] = 1 / h;

        v.add(yLeft);

        for (int i = 1; i < n - 1; i++) {
            double xCur = x.get(i + 1);
            matrix[i][i - 1] = 1 - h * p.apply(xCur) / 2;
            matrix[i][i] = -2 + h*h * q.apply(xCur);
            matrix[i][i + 1] = 1 + h * p.apply(xCur) / 2;

            v.add(h*h * f.apply(xCur));

        }
        double xn = x.get(n - 1);
        matrix[n - 1][n - 2] = -1 / h;
        matrix[n - 1][n - 1] = 1/h - 1;
        v.add(yRight);

        for (int i = 0; i < n; i++) {
            System.out.println(x.get(i) + " " + v.get(i));
        }

        System.out.println(n);
        new Matrix(matrix, n).print();

        return new TridiagonalSolver(new Matrix(matrix, n), v).solve().stream().mapToDouble(i -> i).toArray();
    }



    public static double[] shootingMethod(double left, double right, double yLeft, double yRight, double h, MyFunction<Double> dy, MyFunction<Double> dz) {
        double eps = 0.0001;

        double ang1 = Math.PI / 2;
        double ang2 = -Math.PI / 2;

        double[] first = rungeKuttaMethod(left, right, h, ang1, 1, dy, dz);
        double[] second = rungeKuttaMethod(left, right, h, ang2, 1, dy, dz);

        System.out.println(first[first.length - 1] + "  " + second[second.length - 1]);


        Function<double[], Double> phi = (x) -> {
            return x[x.length - 1] - yRight;
        };

        for (int i = 0; ; i++) {
            double diff = ang2 - ang1;
            if(Math.abs(diff) < eps) {
                System.out.println("Number of iterations: " + i);
                break;
            }
            double tmp = ang2 - phi.apply(second) * diff / (phi.apply(second) - phi.apply(first));
            first = second;
            second = rungeKuttaMethod(left, right, h, tmp, 1, dy, dz);
            ang1 = ang2;
            ang2 = tmp;
        }


        return second;

    }

}
