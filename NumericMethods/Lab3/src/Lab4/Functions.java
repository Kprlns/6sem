package Lab4;

import java.util.function.Function;

import static java.lang.Math.sin;
import static java.lang.Math.cos;
import static java.lang.Math.tan;
import static java.lang.Math.exp;



public class Functions {
    /*
    @FunctionalInterface
    interface MyFunction<A, R> {
        public R apply (A a, A b, A c);
    }

    public static Function<Double, Double> var14 = (x) -> {
        return x;
    };

    public static Function<Double, Double> var14Solution = (x) -> {
        return (-0.9783 * cos(2 * x) + 0.4766 * sin(2 * x)) / sin(x);
    };


    public static MyFunction<Double, Double> dy = (x, y, z) -> z;

    public static MyFunction<Double, Double> dz = (x, y, z) -> {
        return -2 * z / tan(x) - 3 * y;
    };
    */


/*
    public static double dy(double x, double y, double z) {
        return z;
    }
    public static double dz(double x, double y, double z) {
        return -2 * z / tan(x) - 3 * y;
    } */


    @FunctionalInterface
    public interface MyFunction<R> {
        public R apply (R a, R b, R c);
    }

    public static MyFunction<Double> dy = (x, y, z) -> {
       return z;
    };

    public static MyFunction<Double> dz = (x, y, z) -> {
        return -2 * z / tan(x) - 3 * y;
    };

    public static Function<Double, Double> var14Solution = (x) -> {
        return (-0.9783 * cos(2 * x) + 0.4766 * sin(2 * x)) / sin(x);
    };

    public static MyFunction<Double> dz1 = (x, y, z) -> {
      return 2*x*z/(x*x + 1);
    };


    public static Function<Double, Double> f = (x) -> {
        return 0.;
    };

    public static Function<Double, Double> p = (x) -> {
        return -2. / (exp(x) + 1);
    };

    public static Function<Double, Double> q = (x) -> {
        return -exp(x) / (exp(x) + 1);
    };

    public static Function<Double, Double> solution = (x) -> {
        return exp(x) - 1;
    };



    public static MyFunction<Double> shoot = (x, y, z) -> {
        return -2 * z / (exp(x) + 1) - y * (exp(x) / (exp(x) + 1));
    };
}
