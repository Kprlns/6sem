package Lab4;

import java.util.Locale;

import static Lab4.BoundaryTask.Methods.shootingMethod;
import static Lab4.CauchyTask.Methods.rungeRomberg;
import static Lab4.CauchyTask.Methods.rungeRomgerg;
import static Lab4.Functions.*;

public class Lab4 {
    static final double left = 1;
    static final double right = 2;
    static final double h = 0.1;
    static final double y0 = 1;
    static final double z0 = 1;

    static void print(double[] res, double res1[], int p) {
        double x = left;
        double error = 0;
        double[] rr = rungeRomgerg(res, res1, p);
        for (int i = 0; i < (int)((right - left) / h) + 1; ++i) {
            System.out.format(Locale.US, "%.2f:      %.6f  |   %.6f    |  %.6f  |    %.6f     |  %.6f\n",
                    x, res[i], var14Solution.apply(x), Math.abs(res[i] - var14Solution.apply(x)),
                    rungeRomberg(res[i], res1[2 * i], p), Math.abs(rr[i] - var14Solution.apply(x)));

            error += Math.pow(res[i] - var14Solution.apply(x), 2);
            x += h;
        }
        System.out.println("Error: " + Math.pow(error, 1./2.));
    }


    static final double left1 = 0;
    static final double right1 = 1;
    static final double h1 = 0.1;
    static final double yLeft = 1;
    static final double yRight = 1;

    static void print(double[] res) {
        double x = left1;
        double error = 0;
        for (int i = 0; i < (int)((right1 - left1) / h1) + 1; ++i) {
            System.out.format(Locale.US, "%.2f:         %.6f       |    %.6f    |  %.6f   \n",
                    x, res[i], solution.apply(x), Math.abs(res[i] - solution.apply(x)));
            x += h1;
            error += Math.pow(res[i] - solution.apply(x), 2);
        }
        System.out.println("Error: " + Math.pow(error, 1./2.));
    }

    public static void main(String[] args) {
        /*
        double left = 0;
        double right = 1;
        double h = 0.2;
        double y0 = 1;
        double z0 = 3;
        */
/*
        System.out.println("       Euler Method  |  Exact value  |   Error    |  Runge-Romberg  |  Error");
        double[] res = eulerMethod(left, right, h, y0, z0, dy, dz);
        double[] res1 = eulerMethod(left, right, h / 2, y0, z0, dy, dz);
        print(res, res1, 1);


        System.out.println();
        System.out.println("        Runge-Kutta  |  Exact value  |   Error    |  Runge-Romberg  |  Error");
        //System.out.println();
        res = rungeKuttaMethod(left, right, h, y0, z0, dy, dz);
        res1 = rungeKuttaMethod(left, right, h / 2, y0, z0, dy, dz);

        print(res, res1, 4);



        System.out.println();
        System.out.println("       Adams Method  |  Exact value  |    Error   |  Runge-Romberg  |  Error");
        res = adamsMethod(left, right, h, y0, z0, dy, dz);
        res1 = adamsMethod(left, right, h / 2, y0, z0, dy, dz);
        print(res, res1, 4);


        System.out.println("====================================================================================\n");
        System.out.println();
        System.out.println("   Finite Difference method  |  Exact value   |  Error  ");
        double[] kek = finiteDifferenceMethod(//0, 1, 0.2, -1, 3, 0, 2, (x) -> 4 * x / (2 * x + 1), (x) -> -4 / (2 * x + 1), (x) -> 0.);
                 0, 1, h1, 1, 1, 0, -1, p, q, f);
                    //0, 1, h, 1, 1, 1, 0, p, q, f);
        print(kek);


        System.out.println("\n\n\n\n========\n");

        ;

        -0.5 2.5
0.4181818181818181, -1.709090909090909, 1.581818181818182, 0, 0, 0
0, 0.31428571428571417 -1.5428571428571427 1.685714285714286, 0, 0
0, 0, -0.0666666666666671, -0.9333333333333327, 2.0666666666666673, 0
0, 0, 0, 2.5999999999999983 -5.1999999999999975 -0.5999999999999983
-2.5 2.5


*/
        double[] res = shootingMethod(left1, right1, 1, 1, h1, dy, shoot);
        print(res);

    }
}