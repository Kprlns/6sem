package Lab4.CauchyTask;

import Lab1.Pair;
import Lab4.Functions.MyFunction;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Methods {
    private Methods() {}

    public static double[] eulerMethod(double left, double right, double h, double y0, double z0, MyFunction<Double> dy, MyFunction<Double> dz) {
        double[] y = new double[(int)((right - left) / h) + 1];
        y[0] = y0;
        double x = left + h;
        double z = z0;
        int cnt = 1;
        do {
            y[cnt] = y[cnt - 1] + h * dy.apply(x - h,y[cnt - 1], z);
            z += h * dz.apply(x - h,y[cnt - 1],z);
            x += h;
            cnt++;
        } while (x <= right + h/2);

        return y;
    }

    public static double[] eulerCauchyMethod(double left, double right, double h, double y0, double z0, MyFunction<Double> dy, MyFunction<Double> dz) {
        double[] res = new double[(int)((right - left) / h) + 1];
        res[0] = y0;
        double x = left + h;
        double z = z0;
        int cnt = 1;
        do {
            res[cnt] = res[cnt - 1] + h * dy.apply(x - h,res[cnt - 1], z);
            res[cnt] = (res[cnt - 1] + res[cnt]) / 2;
            z += h * dz.apply(x - h,res[cnt - 1],z);
            x += h;
            cnt++;
        } while (x <= right + h/2);

        return res;
    }

    public static double[] improvedEulerMethod(double left, double right, double h, double y0, double z0, MyFunction<Double> dy, MyFunction<Double> dz) {
        double[] y = new double[(int)((right - left) / h) + 1];
        y[0] = y0;
        double x = left + h;
        double z = z0;
        int cnt = 1;
        do {
            double tmp = y[cnt - 1] + h * dy.apply(x - h, y[cnt - 1], z) / 2;
            y[cnt] = tmp + h * dy.apply(x - h - h/2, tmp, z);
            z += h * dz.apply(x - h,y[cnt - 1],z);
            x += h;
            cnt++;
        } while (x <= right + h/2);

        return y;
    }
/*
double tmp = y[cnt - 1] + h * dy.apply(x - h, y[cnt - 1], z) / 2;
            y[cnt] = tmp + h * dy.apply(x - h - h/2, tmp, z);
            z += h * dz.apply(x - h,y[cnt - 1],z);
            x += h;
            cnt++;

                        double tmp = y[cnt - 1] + h * dy.apply(x - h, y[cnt - 1], z) / 2;
            double tmpZ = z + h * dz.apply(x - h, y[cnt - 1], z) / 2;

            y[cnt] = tmp + h * dy.apply(x - h/2, tmp, z);
            z = tmpZ + h * dz.apply(x - h/2, y[cnt - 1] , tmpZ);

 */





    public static double[] rungeKuttaMethod(double left, double right, double h, double y0, double z0, MyFunction<Double> dy, MyFunction<Double> dz) {
        /*
        double[] y = new double[(int)((right - left) / h) + 1];
        y[0] = y0;
        double x = left + h;
        double z = z0;
        int cnt = 1;
        do {
            double k1 = h * dy.apply(x, y[cnt - 1], z);
            double l1 = h * dz.apply(x, y[cnt - 1], z);

            double k2 = h * dy.apply(x + h/2, y[cnt - 1] + k1/2, z + l1/2);
            double l2 = h * dz.apply(x + h/2, y[cnt - 1] + k1/2, z + l1/2);

            double k3 = h * dy.apply(x + h/2, y[cnt - 1] + k2/2, z + l2/2);
            double l3 = h * dz.apply(x + h/2, y[cnt - 1] + k2/2, z + l2/2);

            double k4 = h * dy.apply(x + h, y[cnt - 1] + k3, z + l3);
            double l4 = h * dz.apply( x + h, y[cnt - 1] + k3, z + l3);

            double diffY = (k1 + 2 * k2 + 2 * k3 + k4) / 6;
            double diffZ = (l1 + 2 * l2 + 2 * l3 + l4) / 6;

            y[cnt] = y[cnt - 1] + diffY;
            z += diffZ;
            cnt++;
            x += h;

        } while (x <= right + h/2);
        return y;
        */
        return rungeKutta(left, right, h, y0, z0, dy, dz).first();
    }

    private static Pair<double[]> rungeKutta(double left, double right, double h, double y0, double z0, MyFunction<Double> dy, MyFunction<Double> dz) {
        LinkedList<Double> x = new LinkedList<>();
        LinkedList<Double> y = new LinkedList<>();
        LinkedList<Double> z = new LinkedList<>();
        for (double cur = left; cur <= right + h/3; cur += h) {
            x.add(cur);
        }
        y.add(y0);
        z.add(z0);
        double[] resCoefs = new double[]{1., 2., 2., 1.};
        double[] argsCoefs = new double[]{0., 1 / 2., 1 / 2., 1.};
        for (int i = 1; i < x.size(); i++) {
            double xPrev = x.get(i - 1);
            double yPrev = y.get(i - 1);
            double zPrev = z.get(i - 1);

            double diffY = 0;
            double diffZ = 0;

            double k = 0;
            double l = 0;

            for (int j = 0; j < 4; j++) {
                k = h * dy.apply(xPrev + argsCoefs[j] * h, yPrev + argsCoefs[j] * k, zPrev + argsCoefs[j] * l);
                l = h * dz.apply(xPrev + argsCoefs[j] * h, yPrev + argsCoefs[j] * k, zPrev + argsCoefs[j] * l);
                diffY += k * resCoefs[j];
                diffZ += l * resCoefs[j];
            }
            y.add(yPrev + diffY / 6.);
            z.add(zPrev + diffZ / 6.);
        }
        return new Pair<double[]>(y.stream().mapToDouble(i -> i).toArray(),
                z.stream().mapToDouble(i -> i).toArray());
    }

    public static double[] adamsMethod(double left, double right, double h, double y0, double z0, MyFunction<Double> dy, MyFunction<Double> dz) {
        LinkedList<Double> x = new LinkedList<>();
        LinkedList<Double> y = new LinkedList<>();
        LinkedList<Double> z = new LinkedList<>();

        Pair<double[]> tmp = rungeKutta(left, left + 3*h, h, y0, z0, dy, dz);

        for (double cur = left; cur <= right+ h/2; cur += h) {
            x.add(cur);
        }
        for (int i = 0; i < tmp.first().length; i++) {
            y.add(tmp.first()[i]);
            z.add(tmp.second()[i]);
        }
        double[] coefs = new double[] {55, -59, 37, -9};
        for (int i = 4; i < x.size(); i++) {
            double diffY = 0;
            double diffZ = 0;
            double yPrev = y.get(i - 1);
            double zPrev = z.get(i - 1);

            for (int k = 0; k < 4; k++) {
                double xk = x.get(i - k - 1);
                double yk = y.get(i - k - 1);
                double zk = z.get(i - k - 1);

                diffY += coefs[k] * dy.apply(xk, yk, zk);
                diffZ += coefs[k] * dz.apply(xk, yk, zk);
            }
            y.add(yPrev + h * diffY / 24.);
            z.add(zPrev + h * diffZ / 24.);
        }
        return y.stream().mapToDouble(i -> i).toArray();
    }

    public static double[] rungeRomgerg(double[] y, double[] y2, int p) {
        double[] yNew = new double[y.length];
        double k = Math.pow(2, p) - 1;
        for (int i = 0; i < y.length; i++) {
            yNew[i] = y2[2 * i] + (y2[2 * i] - y[i]) / k;
        }
        return yNew;
    }

    public static double rungeRomberg(double yh, double yh2, int p) {
        double k = Math.pow(2, p) - 1;
        return yh2 + ((yh2 - yh) / k);
    }
}
