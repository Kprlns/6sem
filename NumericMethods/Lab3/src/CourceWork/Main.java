package CourceWork;

import CourceWork.Sparse.SparseMatrix;
import CourceWork.Sparse.SparseVector;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import static CourceWork.Methods.BiCGStab.biCGStab;

public class Main {

    public static void main(String[] args) throws IOException {
        SparseMatrix m = new SparseMatrix();
        //m.readSparce(new BufferedReader(new InputStreamReader(new FileInputStream("src/resources/matrix.txt"))));
        //m.print();
        //BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("src/resources/test1.txt")));
        //BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("src/resources/matrix.txt")));
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("src/resources/test50.txt")));
        m.readSparce(br);
        SparseVector b = new SparseVector(m.size);
        b.readSparce(br);

        m.print();
        b.print();

        SparseVector x0 = new SparseVector(m.size);

        biCGStab(m, b, x0, 0.0005).print();


    }
}
