package CourceWork.Sparse;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public class SparseVector {
    final private static Locale locale = Locale.US;
    final private static String format = "%.4f ";

    Map<Integer, Double> vector;
    int size;

    public SparseVector(int size) {
        this.size = size;
        vector = new TreeMap<>();
    }

    public SparseVector(SparseVector v) {
        this.size = v.size;
        vector = new TreeMap<>();
        for (int i = 0; i < size; ++i) {
            set(i, v.get(i));
        }
    }

    public void readSparce(BufferedReader cin) throws IOException {
        int n = Integer.parseInt(cin.readLine());
        for (int i = 0; i < n; ++i) {
            String[] arr = cin.readLine().split("\\s+");
            set(Integer.parseInt(arr[0]) - 1, Double.parseDouble(arr[1]));
        }
    }

    public void readFull(BufferedReader cin) throws IOException {
        String[] arr = cin.readLine().split("\\s+");
        for (int i = 0; i < arr.length; ++i) {
            set(i, Double.parseDouble(arr[i]));
        }

    }

    public void set(int i, double k) {
        if(i >= size) {
            throw new ArrayIndexOutOfBoundsException(i);
        }
        if(k != 0.0) {
            vector.put(i, k);

        } else {
            vector.remove(i);
        }
    }

    public double get(int i) {
        if(i >= size) {
            throw new ArrayIndexOutOfBoundsException(i);
        }
        double ret;
        try {
            ret = vector.get(i);
        } catch (NullPointerException e) {
            return 0;
        }
        return ret;
    }

    public double multiply(SparseVector vector) {
        double res = 0;
        for(int i = 0; i < size; i++) {
            res += get(i) * vector.get(i);
        }
        return res;
    }

    public SparseVector sub(SparseVector v) {
        SparseVector res = new SparseVector(size);
        for (int i = 0; i < size; i++) {
            res.set(i, get(i) - v.get(i));
        }
        return res;
    }

    public SparseVector add(SparseVector v) {
        SparseVector res = new SparseVector(size);
        for (int i = 0; i < size; i++) {
            res.set(i, get(i) + v.get(i));
        }
        return res;
    }

    public boolean isZero() {
        for (int i = 0; i < size; ++i) {
            if(get(i) != 0.0) {
                return false;
            }
        }
        return true;
    }

    public SparseVector multiply(double k) {
        SparseVector res = new SparseVector(size);
        for (int i = 0; i < size; ++i) {
            res.set(i, get(i) * k);
        }
        return res;
    }

    public double norm() {
        double res = 0;
        for (int i = 0; i < size; i++) {
            double tmp = get(i);
            res += tmp * tmp;
        }
        return Math.sqrt(res);
    }

    public void print() {
        for (int i = 0; i < size; ++i) {
            printDouble(get(i));
            System.out.println();
        }
    }

    private static void printDouble(double d) {
        System.out.format(locale, format, d);
    }
}
