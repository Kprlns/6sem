package CourceWork.Sparse;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

public class SparseMatrix {
    public List<Map<Integer, Double>> matrix;
    public int size;

    final public double eps = 1e-10;
    final private static Locale locale = Locale.US;
    final private static String format = "%.4f ";


    public SparseMatrix(double[][] m, int size) {
        matrix = new ArrayList<>();
        this.size = size;
        for (int i = 0; i < size; ++i) {
            matrix.add(new TreeMap<>());
            for (int j = 0; j < size; ++j) {
                if (m[i][j] != 0.0) {
                    matrix.get(i).put(j, m[i][j]);
                }
            }
        }
    }

    public SparseMatrix(SparseMatrix orig) {
        size = orig.size;
        matrix = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            matrix.add(new TreeMap<>());
            for (Map.Entry<Integer, Double> entry : orig.matrix.get(i).entrySet()) {
                matrix.get(i).put(entry.getKey(), entry.getValue());
            }
        }
    }

    public SparseMatrix(int size) {
        matrix = new ArrayList<>();
        this.size = size;
        for (int i = 0; i < size; ++i) {
            matrix.add(new TreeMap<>());
        }
    }

    public SparseMatrix() {
        matrix = new ArrayList<>();
    }

    public void readSparce(BufferedReader cin) throws IOException {
        size = Integer.parseInt(cin.readLine());

        for (int i = 0; i < size; i++) {
            matrix.add(new TreeMap<>());
        }

        int n = Integer.parseInt(cin.readLine());
        for (int i = 0; i < n; i++) {
            String[] arr = cin.readLine().split("\\s+");
            set(Integer.parseInt(arr[0]) - 1,
                Integer.parseInt(arr[1]) - 1,
                   Double.parseDouble(arr[2]));
        }
    }

    public void readFull(BufferedReader cin) throws IOException {
        size = Integer.parseInt(cin.readLine());
        for (int i = 0; i < size; i++) {
            matrix.add(new TreeMap<>());
        }
        for (int i = 0; i < size; i++) {
            String[] arr = cin.readLine().split("\\s+");
            for (int j = 0; j < arr.length; ++j) {
                set(i, j, Integer.parseInt(arr[i]));
            }
        }
    }


    public void print() {
        for (int i = 0; i < size; ++i) {
            Map<Integer, Double> map = matrix.get(i);
            for (int j = 0; j < size; ++j) {
                printDouble(get(i, j));
            }
            System.out.println();
        }
    }

    public void set(int i, int j, double k) {
        if(i >= size || j >= size) {
            throw new ArrayIndexOutOfBoundsException(i + ", " + j);
        }
        if(k != 0.0) {
            matrix.get(i).put(j, k);
        } else {
            matrix.get(i).remove(j);
        }
    }

    public double get(int i, int j) {
        if(i >= size || j >= size) {
            throw new ArrayIndexOutOfBoundsException(i + ", " + j);
        }
        double ret;
        try {
            ret = matrix.get(i).get(j);
        } catch (NullPointerException e) {
            return 0;
        }
        return ret;
        /*
        Double d;
        d = matrix.get(i).get(j);
        if( d == null ) {
            return 0;
        }
        return d;
        */
    }

    public SparseVector multiply(SparseVector v) {
        SparseVector res = new SparseVector(size);
        for (int i = 0; i < size; ++i) {
            double tmp = 0.0;
            for (int j = 0; j < size; ++j) {
                tmp += get(i, j) * v.get(j);
            }
            if(tmp != 0.0) {
                res.set(i, tmp);
            }
        }
        return res;
    }

    private static void printDouble(double d) {
        System.out.format(locale, format, d);
    }



}
