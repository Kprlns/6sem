package CourceWork.Methods;

import CourceWork.Sparse.SparseMatrix;
import CourceWork.Sparse.SparseVector;


public class BiCGStab {
    private BiCGStab() {

    }


    public static SparseVector biCGStab(SparseMatrix m, SparseVector b, SparseVector x0, double eps) {

        SparseVector r0 = b.sub(m.multiply(x0));
        if(r0.isZero()) {
            return r0;
        }

        int n = 0;
        double alpha, omega, beta;
        SparseVector p = new SparseVector(r0);
        SparseVector r = new SparseVector(r0);
        SparseVector x = new SparseVector(x0);
        SparseVector s;
        do {
            alpha = alpha(m, r0, r, p);
            s = s(m, r, p, alpha);
            omega = omega(m, s);
            x = x(x, p, s, alpha, omega);
            SparseVector rTmp = s.sub(m.multiply(s).multiply(omega));//b.sub(m.multiply(x));
            beta = beta(rTmp, r, r0, alpha, omega);
            p = p(m, rTmp, p, beta, omega);
            r = rTmp;
            ++n;
            /*
            System.out.println(r.norm());
            System.out.println("Alpha: " + alpha);
            System.out.println("Beta: " + beta);
            System.out.println("Omega: " + omega);
            System.out.println("x: ");
            x.print();
            System.out.println("s: ");
            s.print();
            System.out.println("r: ");
            r.print();
            System.out.println("p: ");
            p.print();
            */
        } while (r.norm() > eps);

        System.out.println("Number of iterations: " + n);
        return x;
    }

    private static double alpha(SparseMatrix a, SparseVector r0, SparseVector rk, SparseVector pk) {
        return rk.multiply(r0) / (a.multiply(pk).multiply(r0));
    }
    private static double omega(SparseMatrix a, SparseVector sk) {
        SparseVector tmp = a.multiply(sk);
        return tmp.multiply(sk) / tmp.multiply(tmp);
    }
    private static double beta(SparseVector rk_1, SparseVector rk, SparseVector r0, double alpha, double omega) {
        //return (rk_1.multiply(r0) / rk.multiply(r0);
        return (rk_1.multiply(r0) / rk.multiply(r0)) * (alpha / omega);
    }
    private static SparseVector s(SparseMatrix a, SparseVector rk,  SparseVector pk, double alpha) {
        return rk.sub(a.multiply(pk).multiply(alpha));
    }
    private static SparseVector x(SparseVector x, SparseVector p, SparseVector s, double alpha, double omega) {
        //return x.add(p.multiply(alpha).add(s.multiply(omega)));
        SparseVector tmp1 = p.multiply(alpha);
        SparseVector tmp2 = s.multiply(omega);

        //tmp1.print();
        //tmp2.print();

        return x.add(tmp1).add(tmp2);
    }
    private static SparseVector p(SparseMatrix a, SparseVector rk_1, SparseVector pk, double beta, double omega) {
        //return rk_1.add(pk.multiply(beta).sub(a.multiply(pk).multiply(omega)));
        //SparseVector tmp1 = pk.multiply(beta);
        //SparseVector tmp2 = a.multiply(pk).multiply(-omega);
        //SparseVector tmp3 = tmp1.add(tmp2);
        //return rk_1.add(tmp3);
        SparseVector tmp1 = a.multiply(pk).multiply(-omega);
        SparseVector tmp2 = pk.add(tmp1).multiply(beta);
        return rk_1.add(tmp2);
    }
}
