package Main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(new BufferedReader(new InputStreamReader(System.in)).readLine()));
        int task = Integer.parseInt(br.readLine());

        switch (task) {
            case 1: LUSolution(getMatrix(br), br);
            break;

            case 2: tridiagonalSolution(br);
            break;

            case 3: seidelSolution(getMatrix(br), br);
            break;

            case 4: findEigenvalue(getMatrix(br), br);
            break;

            case 5: QR(getMatrix(br), br);
            break;
        }
    }

    private static void QR(Matrix m, BufferedReader br) throws IOException {

        new QRSolver(m, Double.parseDouble(br.readLine())).find();
    }

    private static void findEigenvalue(Matrix m, BufferedReader br) throws IOException {
        EigenvalueFinder ef = new EigenvalueFinder(m, Double.parseDouble(br.readLine()));
        ef.findU();
    }

    private static void seidelSolution(Matrix m, BufferedReader br) throws IOException {
        ArrayList<Double> b = getVector(br, m.size);
        Double precision = Double.parseDouble(br.readLine());
        SeidelSolver solver = new SeidelSolver(m, b, precision);
        System.out.println("Seidel method:");
        ArrayList<Double> res = solver.solve();
        for (Double d : res) {
            System.out.println(d);
        }
        System.out.println("\nNumber of iterations: " + solver.count);
        System.out.println("\nSimple iterations method:");
        res = solver.simpleItrations();
        for(Double d: res) {
            System.out.println(d);
        }
        System.out.println("\nNumber of iterations: " + solver.count);

    }

    private static void tridiagonalSolution(BufferedReader br) throws IOException {

        int n = Integer.parseInt(br.readLine());
        double[][] matr = new double[n][n];
        for (int i = 0; i < n; i++) {
            String s = br.readLine();
            String[] strs = s.split(" ");
            if( i != 0 && i != n - 1) {
                for (int j = 0; j < 3; j++) {
                    matr[i][i - 1 + j] = Double.parseDouble(strs[j]);
                }
            } else if(i == 0) {
                matr[0][0] = Double.parseDouble(strs[0]);
                matr[0][1] = Double.parseDouble(strs[1]);
            } else {
                matr[n - 1][n - 2] = Double.parseDouble(strs[0]);
                matr[n - 1][n - 1] = Double.parseDouble(strs[1]);
            }
        }

        ArrayList<Double> d = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            d.add(Double.parseDouble(br.readLine()));
        }
        Matrix m = new Matrix(matr,n);

        TridiagonalSolver ts = new TridiagonalSolver(m, d);
        ArrayList<Double> res = ts.solve();

        for (Double x : res) {
            System.out.println(x);
        }


    }

    private static void LUSolution(Matrix m, BufferedReader br) throws IOException {


        Pair<Matrix> matrices = m.LUdecompostion();
        m.doSwaps();

        LUSolver s = new LUSolver(matrices, m);

        ArrayList<Double> b = getVector(br, m.size);

        System.out.println("Solution:");
        ArrayList<Double> res = s.solve(b);
        for (Double re : res) {
            System.out.println(re + " ");
        }
        System.out.println("\nDeterminant: " + s.determinant());

        m.doRevarseSwap();
        Matrix inv = m.inversion();

        System.out.println("\nInverted matrix:");
        inv.print();
        System.out.println("\nCheck:");
        inv.multiply(m).print();

    }

    static Matrix getMatrix(BufferedReader br) throws IOException {
        int n = Integer.parseInt(br.readLine());
        double[][] matr = new double[n][n];
        for (int i = 0; i < n; i++) {
            String s = br.readLine();
            String[] strs = s.split(" ");
            for (int j = 0; j < n; j++) {
                matr[i][j] = Double.parseDouble(strs[j]);
            }
        }
        return new Matrix(matr, n);
    }

    static ArrayList<Double> getVector(BufferedReader br, int n) throws IOException {
        ArrayList<Double> b = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            b.add(Double.parseDouble(br.readLine()));
        }
        return b;
    }
}
