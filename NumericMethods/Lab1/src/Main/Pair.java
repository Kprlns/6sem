package Main;

public class Pair<T> {

    private T first;
    private T second;
    Pair(T f, T s) {
        first = f;
        second = s;
    }

    public Pair() {

    }

    T first() {
        return first;
    }
    T second() {
        return  second;
    }


    boolean equal(Pair<T> p) {
        return (first == p.first) && (second == p.second);
    }

    void setFirst(T first) {
        this.first = first;
    }

    void setSecond(T second) {
        this.second = second;
    }

    void set(T first, T second) {
        this.first = first;
        this.second = second;
    }

}
