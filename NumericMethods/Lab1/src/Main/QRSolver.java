package Main;

public class QRSolver {
    Matrix m;
    double eps;

    QRSolver(Matrix m, double e) {
        this.m = m;
        eps = e;
    }

    public void find() {
        int cnt = 0;
        double[] real = new double[m.size];
        double[] real1 = new double[m.size];
        double[] complex = new double[m.size];
        double[] complex1 = new double[m.size];
        do {
            for (int i = 0; i < m.size; i++) {
                real1[i] = real[i];
                complex1[i] = complex[i];
            }

            Pair<Matrix> pair = m.QRdecompostion();
            Matrix tmp = pair.second().multiply(pair.first());


            int n = m.size;
            for (int i = 0; i < n; i++) {
                if(i >= n - 1 || tmp.matrix[i + 1][i] * tmp.matrix[i + 1][i] < eps) {

                    real[i] = tmp.matrix[i][i];
                    complex[i] = 0;
                } else {
                    double k1 = tmp.matrix[i][i];
                    double k2 = tmp.matrix[i + 1][i + 1];
                    double c = tmp.matrix[i][i + 1] * tmp.matrix[i + 1][i];
                    double D = (k1 + k2) * (k1 + k2) - 4 * (k1 * k2 - c);
                    if(D < 0) {

                        real[i] = (k1 + k2) / 2;
                        complex[i] = Math.sqrt(Math.abs(D)) / 2;

                        real[i + 1] = (k1 + k2) / 2;
                        complex[i + 1] = -1 * Math.sqrt(Math.abs(D)) / 2;
                    } else {
                        real[i] = (k1 + k2) / 2 + Math.sqrt(Math.abs(D)) / 2;
                        complex[i] = 0;

                        real[i + 1] = (k1 + k2) / 2 - Math.sqrt(Math.abs(D));
                        complex[i + 1] = 0;
                    }
                    i++;
                }
            }
            m = tmp;
            cnt++;

        } while (check(real, real1, complex, complex1));

        for (int i = 0; i < m.size; i++) {
            System.out.println(real[i] + " + " + complex[i] + "i");
        }
        System.out.println("\nNumber of iterations: " + cnt);
    }

    private boolean check(double[] real, double[] real1, double[] complex, double[] complex1) {
        for (int i = 0; i < m.size; i++) {
            Pair<Double> c = new Pair<>(real[i], complex[i]);
            Pair<Double> c1 = new Pair<>(real1[i], complex1[i]);

            double a = real[i] - real1[i];
            double b = complex[i] - complex1[i];

            if(Math.sqrt(a*a + b*b) > eps) {
                return true;
            }
        }
        return false;
    }
    
}
