package Lab2;
import Lab1.Matrix;
import Lab1.Pair;

public class NewtonMethodSystem {
    double eps;

    double leftX1;
    double leftX2;
    double rightX1;
    double rightX2;

    int count;


    public NewtonMethodSystem(double leftX1, double rightX1, double leftX2, double rightX2, double eps) {
        this.eps = eps;
        this.leftX1 = leftX1;
        this.rightX1 = rightX1;
        this.leftX2 = leftX2;
        this.rightX2 = rightX2;
        count = 0;
    }

    public Pair<Double> solve() {
        double curX1 = leftX1 + (rightX1 - leftX1) / 2;
        double prevX1;
        double curX2 = leftX2 + (rightX2 - leftX2) / 2;
        double prevX2;

        do {
            count++;

            prevX1 = curX1;
            prevX2 = curX2;

            Matrix j = genJ(curX1, curX2);
            Matrix a1 = genA1(curX1, curX2);
            Matrix a2 = genA2(curX1, curX2);


            curX1 -= (a1.determinant() / j.determinant());
            curX2 -= (a2.determinant() / j.determinant());

        } while ( (Math.abs(curX1 - prevX1)) > eps && (Math.abs(curX2 - prevX2)) > eps);

        return new Pair<>(curX1, curX2);
    }

    double f1(double x1, double x2) {
        return ((Math.pow(x1, 2) + 4 * Math.pow(x2, 2)) / 9) - 1;
    }

    double f2(double x1, double x2) {
        return 3 * x2 - Math.exp(x1) - x1;
    }

    double df1_dx1(double x1) {
        return 2 * x1 / 9;
    }

    double df1_dx2(double x2) {
        return 8 * x2 / 9;
    }

    double df2_dx1(double x1) {
        return -Math.exp(x1) - 1;
    }

    double df2_dx2(double x2) {
        return 3;
    }

    Matrix genJ(double x1, double x2) {
        double[][] arr = new double[2][2];
        arr[0][0] = df1_dx1(x1);
        arr[0][1] = df1_dx2(x2);
        arr[1][0] = df2_dx1(x1);
        arr[1][1] = df2_dx2(x2);

        return new Matrix(arr, 2);
    }

    Matrix genA1(double x1, double x2) {
        double[][] arr = new double[2][2];
        arr[0][0] = f1(x1, x2);
        arr[0][1] = df1_dx2(x2);
        arr[1][0] = f2(x1, x2);
        arr[1][1] = df2_dx2(x2);

        return new Matrix(arr, 2);
    }

    Matrix genA2(double x1, double x2) {
        double[][] arr = new double[2][2];
        arr[0][0] = df1_dx1(x1);
        arr[0][1] = f1(x1, x2);
        arr[1][0] = df2_dx1(x1);
        arr[1][1] = f2(x1, x2);

        return new Matrix(arr, 2);
    }
}
