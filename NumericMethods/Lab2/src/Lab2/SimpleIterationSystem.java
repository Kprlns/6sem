package Lab2;

import Lab1.Pair;

public class SimpleIterationSystem {
    double eps;

    double leftX1;
    double leftX2;
    double rightX1;
    double rightX2;

    int count;


    public SimpleIterationSystem(double leftX1, double rightX1, double leftX2, double rightX2, double eps) {
        count = 0;
        this.eps = eps;
        this.leftX1 = leftX1;
        this.rightX1 = rightX1;
        this.leftX2 = leftX2;
        this.rightX2 = rightX2;

    }

    public Pair<Double> solve() {
        final double q = 0.45;
        double curX1 = leftX1 + (rightX1 - leftX1) / 2;
        double prevX1;
        double curX2 = leftX2 + (rightX2 - leftX2) / 2;
        double prevX2;

        double cmp;
        do {
            count++;
            prevX1 = curX1;
            prevX2 = curX2;

            curX1 = f1(curX1, curX2);
            curX2 = f2(curX1, curX2);

            cmp = Math.max( Math.abs(curX1 - prevX1) , Math.abs(curX2 - prevX2));

        } while (cmp * (q / (1 - q)) > eps);

        return new Pair<>(curX1, curX2);
    }

    double f1(double x1, double x2) {
        return Math.log(3 * x2 - x1);
    }
    double f2(double x1, double x2) {
        return Math.sqrt(9 - x1 * x1) / 2;
    }
}
