package Lab2;

public class NewtonMethod {
    double eps;
    double x0;
    int cnt;

    NewtonMethod(double x0, double eps) {
        this.eps = eps;
        this.x0 = x0;
        cnt = 0;
    }

    double f(double x) {
        return Math.pow(x, 3) - 2 * Math.pow(x, 2) - 10 * x + 15;
    }

    double df(double x) {
        return 3 * Math.pow(x, 2) - 4 * x - 10;
    }

    double solve() {
        double cur = x0;
        double prev;
        do {
            cnt++;
            prev = cur;
            cur = prev - f(prev) / df(prev);
        } while (Math.abs(cur - prev) > eps);

        return cur;
    }
}
