package Lab1;

public class ComplexNumber {
    static Pair<Double> sqrt(double num) {
        if(num < 0) {
            return new Pair<>(0.0, Math.sqrt(Math.abs(num)));
        } else {
            return new Pair<>(Math.sqrt(num), 0.0);
        }
    }

    static Pair<Double> sum(Pair<Double> n1, Pair<Double> n2) {
        return new Pair<>(n1.first() + n2.first(), n1.second() + n2.second());
    }

    static Pair<Double> sum(Pair<Double> c, double d) {
        return new Pair<>(c.first() + d, c.second());
    }

    static Pair<Double> mul(Pair<Double> c, double d) {
        return new Pair<>(c.first() * d, c.second() * d);
    }

    static double abs(Pair<Double> c) {
        return Math.sqrt(c.first() * c.first() + c.second() * c.second());
    }

}
