package Lab1;

import java.util.ArrayList;

public class TridiagonalSolver {

    Matrix matrix;
    ArrayList<Double> d;

    ArrayList<Double> c;
    ArrayList<Double> f;

    TridiagonalSolver(Matrix m, ArrayList<Double> b) {
        matrix = m;
        d = b;
        c = new ArrayList<>();
        f = new ArrayList<>();
    }

    ArrayList<Double> solve() {
        count();
        return solution();
    }

    private void count() {
        double[][] matrix = this.matrix.matrix;

        c.add(matrix[0][1] / matrix[0][0]);
        f.add(d.get(0) / matrix[0][0]);
        for (int i = 1; i < this.matrix.size; i++) {
            if(i < this.matrix.size - 1) {
                c.add(matrix[i][i + 1] / (matrix[i][i] - matrix[i][i - 1]*c.get(i - 1)));
            }
            f.add( (d.get(i) - matrix[i][i - 1]*f.get(i - 1)) / (matrix[i][i] - matrix[i][i - 1]*c.get(i - 1)) );
        }
    }

    private ArrayList<Double> solution() {
        ArrayList<Double> res = new ArrayList<>();
        for (int i = 0; i < matrix.size; i++) {
            res.add(0.0);
        }

        res.set(matrix.size - 1, f.get(f.size() - 1));

        for (int i = f.size() - 2; i >= 0; i--) {
            res.set(i, f.get(i) - c.get(i) * res.get(i + 1));
        }

        return res;
    }
}
