package Lab1;

public class QuadricEquation {
    static Pair<Pair<Double>> solve(double ii, double ij, double ji, double jj) {

        double b = jj + ii;

        double c = ij*ji;

        double d = b*b - 4 * (ii * jj - ij * ji);

        Pair<Double> dis = ComplexNumber.sqrt(d);

        Pair<Double> x1 = ComplexNumber.sum(dis, -b);
        x1 = ComplexNumber.mul(x1, 0.5);

        Pair<Double> x2 = ComplexNumber.sum(ComplexNumber.mul(dis, -1), -b);
        x2 = ComplexNumber.mul(x2, 0.5);

        return new Pair<>(x1, x2);
    }
}
